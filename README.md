# Antigravity

## Game flow
**Main Menu**
- Controls which menu is shown, and can initiate loading of different levels.

**Loader**
- Called by my menu. It shows some text while doing an asynchronous loading of the level.

**EndGame**
- Shows score on end-game screen


## Game logic
**CubbieData**

**ScorePreservation**
- Tied to a static object that lives throughout the whole game.
- Stores score/points for all levels (Will be responsible for writing to files).

**LevelData**


## Movement
**PlayerControls**
- Defines player movement.
- Controls transparency of the shield and other player features.

**CameraFollow**
- Used by main camera on all Levels. Describes how the camera should follow the player.

**Rotate**
- Used by any object that rotates 


## UX
**Music**
- Tied to *Game* object and loops music.

**Sounds**
- Tied to player and plays FX sound on collision.