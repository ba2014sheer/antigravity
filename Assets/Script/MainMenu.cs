﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Advertisements;

public class MainMenu : MonoBehaviour {
    //public Text[] scoreTable;
    //public Text[] progressTable;
    public Text[] outOfFive;
    public Text[] difficulty;
    public Button[] levelButtons;
    public Button[] rights;
    public Button endless;
    public Text totalLuminas;
    public GameObject cubbieMenu;
    public Text maxLevelText;
    public Text totalScoreText;
    public Text note;
    private int maxLevel;
    private int levelToLoad = 0;
    private readonly int AD_BONUS = 7;

    void Awake() {
        //Advertisement
        Advertisement.Initialize("3412184");
        cubbieMenu.SetActive(true);
        LoadPrefs();
    }

    private void LoadPrefs() {
        if (PlayerPrefs.GetInt("First") == 0) {
            for (int j = 0; j < ScorePreservation.levelsDifficulty.Length; j++) {
                PlayerPrefs.SetFloat("difficulty" + j, 1.0f);
                ScorePreservation.levelsDifficulty[j] = 1;
            }
            PlayerPrefs.SetInt("First", 1);
        }

        ScorePreservation.maximumLevel = PlayerPrefs.GetInt("level");
        ScorePreservation.totalLuminas = PlayerPrefs.GetInt("luminas");
        ScorePreservation.totalScore = PlayerPrefs.GetInt("totalScore");

        for (int i = 0; i < ScorePreservation.levelsScore.Length; i++) {
            ScorePreservation.levelsScore[i] = PlayerPrefs.GetInt("score" + i);
            ScorePreservation.levelsProgress[i] = PlayerPrefs.GetInt("progress" + i);
            ScorePreservation.levelsDifficulty[i] = PlayerPrefs.GetFloat("difficulty" + i);
        }
    }

    public void LevelsButton() {
        maxLevel = ScorePreservation.maximumLevel;
        for (int i = 0; i <= maxLevel; i++) {
            levelButtons[i].interactable = true;
            levelButtons[i].GetComponentInChildren<Text>().color = new Color(0.3137255f, 0.7843137f, 0.7843137f, 1);
        }
        if (maxLevel > 5) {
            rights[0].interactable = true;
            rights[0].GetComponentInChildren<Text>().color = new Color(0.3137255f, 0.7843137f, 0.7843137f, 1);
        }
        if (maxLevel > 11) {
            rights[1].interactable = true;
            rights[1].GetComponentInChildren<Text>().color = new Color(0.3137255f, 0.7843137f, 0.7843137f, 1);
        }
        if (maxLevel > 17) {
            rights[2].interactable = true;
            rights[2].GetComponentInChildren<Text>().color = new Color(0.3137255f, 0.7843137f, 0.7843137f, 1);
        }
        if (maxLevel > 23) {
            rights[3].interactable = true;
            rights[3].GetComponentInChildren<Text>().color = new Color(0.3137255f, 0.7843137f, 0.7843137f, 1);
        }
    }

    public void LoadLevel(int level) {
        note.text = stories[level];
        levelToLoad = level;
        cubbieMenu.SetActive(false);
    }

    public void StartLevel() {
        StartCoroutine(LoadYourAsyncScene(levelToLoad));
    }

    IEnumerator LoadYourAsyncScene(int level) {
        ScorePreservation.level = level;
        AsyncOperation asyncLoad;
        if (level == 5 || level == 11 || level == 17 || level == 23 || level == 29) {
            asyncLoad = SceneManager.LoadSceneAsync("LevelDarkness");
        } else {
            asyncLoad = SceneManager.LoadSceneAsync("LevelStandard");
        }
        // Wait until the asynchronous scene fully loads
        while (!asyncLoad.isDone) {
            yield return null;
        }
    }

    public void StoryMode() {
        totalLuminas.text = ScorePreservation.totalLuminas + "";
    }

    public void ProgressButton() {
        maxLevelText.text = ScorePreservation.maximumLevel + "/30";
        totalScoreText.text = ScorePreservation.totalScore + "";
        int cnt = ScorePreservation.maximumLevel;
        int i = 0;
        while (cnt >= 6) {
            cnt -= 6;
            outOfFive[i].text = "6/6";
            outOfFive[i].color = new Color(0.3137255f, 0.7843137f, 0.7843137f, 1);
            i++;
        }
        if (i < outOfFive.Length) {
            outOfFive[i].text = cnt + "/6";
        }
    }

    public void DifficultyMenu() {
        for (int k = 0; k < 30; k++) {
            difficulty[k].text = k + ": " + ScorePreservation.levelsDifficulty[k];
        }
    }

    public void UnlockAll() {
        ScorePreservation.maximumLevel = 29;
    }

    public void Credits() {
        SceneManager.LoadSceneAsync("Credits");
    }

    public void ProgressReset() {
        PlayerPrefs.DeleteAll();
        ScorePreservation.maximumLevel = 0;
    }

    public void PlayAd() {
        ShowOptions so = new ShowOptions {
            resultCallback = DemandAd
        };
        Advertisement.Show("rewardedVideo", so);
    }

    private void DemandAd(ShowResult sr) {
        if (sr == ShowResult.Finished) {
            ScorePreservation.totalLuminas = PlayerPrefs.GetInt("luminas") + AD_BONUS;
            PlayerPrefs.SetInt("luminas", ScorePreservation.totalLuminas);
            StoryMode();
        }
    }

    public void ExitButton() {
        Application.Quit();
    }

    private readonly string [] stories = {
        "Around 7% of players can't pass level 0.\nNo pressure.",
        "If you are reading this you obviously understood the controls.\nHere is where the game actually starts.",
        "You'll see tiny red things flying around.\nThose are called Luminas.\nEveryone wants them!",
        "Luminas are precious but difficult to catch.\nDon't die chasing them.",
        "The game runs dynamic difficulty algorithm that uses mathematical formulas to ensure...\n Who cares, doesn't matter.",
        "When darkness comes Luminas keep your path illuminated!\nIf you run out of Luminas you'll enter complete darkness.",

        "Congratulations on passing the prologue!\nNow we're talking!",
        "Is there a meaning to this game?",
        "Details matter.",
        "Hint: Obstacles move just to one side each level.",
        "Do you ever overthink?",
        "Ok, I'll let you concentrate now.",

        "If you feel this game is a little repetitive,\nyou don't want to know about people's lives.",
        "If see this note for more than 30 times,\nI'd suggest you lie down for a bit",
        "Just tell me the truth.",
        "The Cube changed colour slightly from the beginning.\nDid you notice that?",
        "There is a meaning.",
        "No more talking. Blazing rocks are flying.",

        "How fast is too fast?",
        "\"It's not only the scenery you miss, you can also miss the sense of where you are going and why.\"",
        "This can really test your reflexes.",
        "I'll become ill if you remove my apostrophe.\n Get it?",
        "We have art in order not to die of the truth.",
        "If we are always moving forward\n and the future cannot be seen,\n how do we find the strength\n to confront it?",

        "The things that attract you can sometimes be harmful.",
        "Let me tell you a short story...",
        "She never felt stressed.",
        "Creation is easy.",
        "I handle social situations with ease.", //Move down
        "This is not scary.", //Move down
        //courage and validation.
    };
}
