﻿using UnityEngine;

public class ScorePreservation : MonoBehaviour {

    public static int[] levelsScore = new int[30];
    public static int[] levelsProgress = new int[30];
    public static float[] levelsDifficulty = new float[30];
    public static int maximumLevel = 1;
    public static int totalLuminas = 0;
    public static int addScore = 0;
    public static int totalScore = 0;
    public static int currentScore = 0;
    public static int collectedLuminas = 0;
    public static int currentProgress = 0;
    public static int level = 0;

    public static void UpdateScore(int score, int progress, int luminas) {
        if (progress >= 99) {
            if (level + 1 > maximumLevel) {
                maximumLevel = level + 1;
                addScore = score;
            }
        }
        currentScore = score;
        currentProgress = progress;
        collectedLuminas = luminas;
        if (score > levelsScore[level]) {
            levelsScore[level] = score;
        }
        if (progress > levelsProgress[level]) {
            levelsProgress[level] = progress;
        }
    }
}
