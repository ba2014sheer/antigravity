﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControls : MonoBehaviour {
    public GameObject shield;
    private Rigidbody rb;
    public float drift;
    public float speed;
    public float rotation;
    // Use this for initialization
    private Rigidbody shieldRb;
    private Renderer shieldRenderer;

    void Start() {
        rb = GetComponent<Rigidbody>();
        rb.AddTorque(new Vector3(rotation, 2 * rotation, 3 * rotation));
        rb.maxAngularVelocity = 10;
        shieldRb = shield.GetComponent<Rigidbody>();
        shieldRenderer = shield.GetComponent<Renderer>();
        Color c = shieldRenderer.material.color;
        c.a = 0.0f;
        shieldRenderer.material.color = c;
        if (ScorePreservation.level >= 18 && ScorePreservation.level <= 23) {
            speed *= 1.8f;
            rotation *= 2f;
            drift *= 1.8f;
        }
        if (ScorePreservation.level >= 24 && ScorePreservation.level <= 29) {
            speed *= 1.2f;
            rotation *= 1.5f;
            drift *= 1.2f;
        }
    }

    // Update is called once per frame
    void FixedUpdate() {
        rb.AddForce(new Vector3(0, 0, speed));
        shieldRb.AddTorque(3 * rotation, 2 * rotation, rotation);

        if (Input.touches.Length > 0 && rb.position.y == 0) {
            Touch t = Input.touches[0];
            if (t.position.x - Screen.width / 2 > 0) {
                rb.AddForce(new Vector3(drift, 0, 0), ForceMode.Impulse);
            } else {
                rb.AddForce(new Vector3(-drift, 0, 0), ForceMode.Impulse);
            }
        }
        Keyboard();
    }

    void OnCollisionEnter(Collision other) {
        if (other.gameObject.CompareTag("Collectable")) {
            Color c = shieldRenderer.material.color;
            c.a = 0.5f;
            shieldRenderer.material.color = c;
        }
        if (other.gameObject.CompareTag("Obsticle")) {
            Color c = shieldRenderer.material.color;
            c.a -= 0.3f;
            shieldRenderer.material.color = c;
        }
    }

    void Keyboard() {
        if (Input.GetKeyDown("left")) {
            rb.AddForce(new Vector3(-drift * 8, 0, 0), ForceMode.Impulse);
        }
        if (Input.GetKeyDown("right")) {
            rb.AddForce(new Vector3(drift * 8, 0, 0), ForceMode.Impulse);
        }
    }

}
