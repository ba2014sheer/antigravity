﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SwipeDetector : MonoBehaviour {
    private Vector2 fingerDownPosition;
    private Vector2 fingerUpPosition;
    private float minDistanceForSwipe = 20f;
    public static event Action<SwipeData> OnSwipe = delegate {
    };
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        foreach (Touch touch in Input.touches) {
            if (touch.phase == TouchPhase.Began) {
                fingerDownPosition = touch.position;
                fingerUpPosition = touch.position;
            }
            if (touch.phase == TouchPhase.Ended) {
                fingerDownPosition = touch.position;
                DetectSwipe();
            }
        }
    }

    private void DetectSwipe() {
        if (SwipeDistanceCheckMet()) {
            if (IsHorizontalSwipe()) {
                var direction = fingerDownPosition.x - fingerUpPosition.x > 0 ? SwipeDirection.Right : SwipeDirection.Left;
                SendSwipe(direction);
            }
            fingerUpPosition = fingerDownPosition;
        }
    }

    private bool IsHorizontalSwipe() {
        return HorizontalMovementDistance() > VerticalMovementDistance();
    }

    private bool SwipeDistanceCheckMet() {
        return VerticalMovementDistance() > minDistanceForSwipe || HorizontalMovementDistance() > minDistanceForSwipe;
    }

    private float VerticalMovementDistance() {
        return Mathf.Abs(fingerDownPosition.y - fingerUpPosition.y);
    }

    private float HorizontalMovementDistance() {
        return Mathf.Abs(fingerDownPosition.y - fingerUpPosition.y);
    }

    private void SendSwipe(SwipeDirection direction) {
        SwipeData swipeData = new SwipeData() {
            direction = direction,
            startPostion = fingerDownPosition,
            endPosition = fingerUpPosition
        };
        OnSwipe(swipeData);
    }
}

public struct SwipeData {
    public Vector2 startPostion;
    public Vector2 endPosition;
    public SwipeDirection direction;
}

public enum SwipeDirection {
    Left, Right
}