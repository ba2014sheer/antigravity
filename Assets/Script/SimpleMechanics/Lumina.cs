﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lumina : MonoBehaviour {
    private Rigidbody rb;
    private float pushTimerH;
    private float pushTimerV;
    private float pushForceH;
    private float pushForceV;

    // Start is called before the first frame update
    void Start() {
        rb = GetComponent<Rigidbody>();
        Random.InitState(System.DateTime.Now.Millisecond);
        pushTimerH = 2f;
        pushTimerV = 2f;
        pushForceH = 3f;
        pushForceV = 3f;
    }

    // Update is called once per frame
    void FixedUpdate() {
        rb.AddForce(new Vector3(pushForceH, 0, pushForceV));
        pushTimerH -= Time.deltaTime;
        pushTimerV -= Time.deltaTime;
        if (pushTimerH < 0) {
            pushTimerH = Random.Range(0.5f, 2f);
            pushForceH = Random.Range(-0.8f, 0.8f);
        }
        if (pushTimerV < 0) {
            pushTimerV = Random.Range(1f, 3f);
            pushForceV = Random.Range(-1.2f, 3f);
        }
        Restrain();
    }

    void Restrain() {
        if (rb.position.x > 15f) {
            rb.position = new Vector3(-15, rb.position.y, rb.position.z + 10);
        }
        if (rb.position.x < -15f) {
            rb.position = new Vector3(15, rb.position.y, rb.position.z + 10);
        }
    }

}
