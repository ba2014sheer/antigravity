﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Music : MonoBehaviour {
    public AudioClip[] clips;
    private AudioSource source;
    private int toggle;

    // Use this for initialization
    void Start() {
        source = GetComponent<AudioSource>();
        toggle = PlayerPrefs.GetInt("music");
    }

    // Update is called once per frame
    void Update() {
        if (!source.isPlaying && toggle == 0) {
            source.PlayOneShot(clips[Random.Range(0, clips.Length)]);
        }
        if (toggle == 1) {
            source.Stop();
        }
    }

    public void ToggleMusic() {
        int newToggle = toggle == 1 ? 0 : 1;
        toggle = newToggle;
        PlayerPrefs.SetInt("music", toggle);
    }
}
