﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CubbieData : MonoBehaviour {
    public Text agValueBox;
    public Text agTextBox;
    public Text dstBox;
    public Text luminasBox;
    public Text luminasTxt;
    public Text message;
    public Text scoreBox;
    public Text warning;

    public static int shield;
    public static int antigravity;
    public GameObject lightObj;
    private Light myLight;
    private int levelLen;
    private int luminas;
    private int score;
    private int progress;
    private int lvl;

    private bool dimming = false;

    private Rigidbody rb;
    // Use this for initialization
    void Start() {
        rb = GetComponent<Rigidbody>();
        levelLen = GameObject.Find("Game").GetComponent<LevelSetup>().GetTotalLevelLen();
        shield = 0;
        antigravity = 1;
        luminas = 0;
        score = 0;
        progress = 0;
        InvokeRepeating("SetScore", 1f, 0.2f);

        lvl = ScorePreservation.level;

        if (lvl != 0) {
            message.text = "";
        }
        if (lvl == 5 || lvl == 11 || lvl == 17 || lvl == 23 || lvl == 29) {
            myLight = lightObj.GetComponent<Light>();
            luminas = ScorePreservation.totalLuminas;
            luminasTxt.text = "Luminas left:";
            luminasBox.text = "" + luminas;
            InvokeRepeating("LuminasLost", 2f, 2f);
        }
    }

    void FixedUpdate() {
        if (antigravity <= 0) {
            agTextBox.text = "WARNING:";
            agValueBox.text = "";
            warning.text = "Cannot resist gravity";
        } else {
            agTextBox.text = "Antigravity shield";
            if (shield == 2) {
                agValueBox.text = "Strong";
            } else if (shield == 1) {
                agValueBox.text = "Weakened";
            } else {
                agValueBox.text = "Broken";
            }
            progress = (int)rb.position.z * 100 / levelLen;
            ScorePreservation.UpdateScore(score, progress, luminas);
        }

        Collider[] hitColliders = Physics.OverlapSphere(transform.position, 12);
        int i = 0;
        //if (lvl != 28) {
            while (i < hitColliders.Length) {
                if (hitColliders[i].gameObject.name.Contains("Attractive") && antigravity > 0) {
                    rb.AddForce((hitColliders[i].gameObject.transform.position - transform.position).normalized * (12f / 30f + Vector2.Distance(new Vector3(hitColliders[i].gameObject.transform.position.x, 0, hitColliders[i].gameObject.transform.position.z), transform.position)), ForceMode.Impulse);
                } else if (hitColliders[i].gameObject.name.Contains("Repellent") && antigravity > 0) {
                    rb.AddForce(-(hitColliders[i].gameObject.transform.position - transform.position).normalized * (12f / 30f + Vector2.Distance(new Vector3(hitColliders[i].gameObject.transform.position.x, 0, hitColliders[i].gameObject.transform.position.z), transform.position)), ForceMode.Impulse);
                }
                i++;
         //}
        }
    }

    void LuminasLost() {
        if (luminas > 0) {
            luminas--;
        } else {
            if (dimming == false) {
                dimming = true;
                InvokeRepeating("DimLight", 2f, 5f);
            }
        }
    }

    void DimLight() {
        myLight.intensity--;
        myLight.intensity--;
    }

    void OnCollisionEnter(Collision other) {
        if (other.gameObject.CompareTag("Lumina")) {
            other.gameObject.SetActive(false);
            luminas++;
            score += 45;
        }
        if (other.gameObject.CompareTag("Collectable")) {
            other.gameObject.SetActive(false);
            if (shield == 2) {
                score += 65;
            }
            if (shield == 1) {
                score += 15;
            }
            shield = 2;
            SetScore();
        }
        if (other.gameObject.CompareTag("Obsticle")) {
            if (shield > 0) {
                shield--;
            } else {
                antigravity = 0;
                rb.AddForce(-(other.gameObject.transform.position - transform.position).normalized * 200f, ForceMode.Impulse);
                other.gameObject.GetComponent<Collider>().enabled = false;
                Invoke("DisableAntigravity", 0.8f);
            }
        }
    }

    void DisableAntigravity() {
        rb.constraints = RigidbodyConstraints.None;
        rb.useGravity = true;
        gameObject.GetComponent<BoxCollider>().enabled = false;
    }

    void SetScore() {
        if (rb.position.z > 0) {
            if (lvl == 0) {
                message.text = "";
            }
            score++;
            dstBox.text = "" + progress + "%";
            if (ScorePreservation.level >= 2) {
                if (lvl == 5 || lvl == 11 || lvl == 17 || lvl == 23 || lvl == 29) {
                    luminasTxt.text = "Luminas left:";
                    luminasBox.text = "" + luminas;
                } else {
                    luminasTxt.text = "Luminas:";
                    luminasBox.text = "" + luminas;
                }
            }
        }
        scoreBox.text = "" + score;
    }
}


