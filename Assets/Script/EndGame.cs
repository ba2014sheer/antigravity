﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class EndGame : MonoBehaviour {
    public Text scoreText;
    public Text progressText;
    public Text luminasText;
    public Text message;
    private int score;
    private int progress;
    private readonly int LUM_SCORE_FACT = 160;

    // Use this for initialization
    void Awake() {
        score = ScorePreservation.currentScore;
        progress = ScorePreservation.currentProgress;

        scoreText.text = score.ToString();
        progressText.text = progress.ToString() + '%';
        if (ScorePreservation.level == 5 || ScorePreservation.level == 11 || ScorePreservation.level == 17 || ScorePreservation.level == 23 || ScorePreservation.level == 29) {
            ScorePreservation.totalLuminas = ScorePreservation.collectedLuminas;
            luminasText.text = "\t\t" + ScorePreservation.totalLuminas + " luminas left";
        } else if (ScorePreservation.level < 2) {
            luminasText.text = "";
        } else {
            int acquiredLuminas = 0;
            acquiredLuminas += ScorePreservation.collectedLuminas;
            acquiredLuminas += score / LUM_SCORE_FACT;
            int levelCompletedBonus = 0;
            if (progress >= 99) {
                levelCompletedBonus = (int)(ScorePreservation.collectedLuminas * 0.5);
            }
            acquiredLuminas += levelCompletedBonus;
            luminasText.text = "+ " + ScorePreservation.collectedLuminas + " luminas (collected)\n" +
                               "+ " + levelCompletedBonus + " luminas (level completed)\n" +
                               "+ " + score / LUM_SCORE_FACT + " luminas (score)\n" +
                "   " + acquiredLuminas + " total acquired";

            ScorePreservation.totalLuminas += acquiredLuminas;
        }

        int lvl = ScorePreservation.level;
        if (progress >= 99) {
            if (lvl < 29) {
                message.text = "Well done!";
            } else {
                message.text = "TO BE CONTINUED...";
            }
        } else {
            message.text = "I know you can do better!";
        }

        if (progress >= 99 && lvl < 29 && (lvl + 1 == ScorePreservation.maximumLevel)) {
            ScorePreservation.levelsDifficulty[lvl + 1] = (float)Math.Round(ScorePreservation.levelsDifficulty[lvl] + 0.25, 2);
        }

        if (progress < 99 && (lvl == ScorePreservation.maximumLevel)) {
            // Completely magic formula
            ScorePreservation.levelsDifficulty[lvl] = (float)Math.Round(ScorePreservation.levelsDifficulty[lvl] - (ScorePreservation.levelsDifficulty[lvl] * (0.9 - (float)progress / 70f) * 0.1f), 2);
        }
        SaveGame();
    }

    // Update is called once per frame
    public void Continue() {
        if (ScorePreservation.level == 29 && progress >= 99) {
            SceneManager.LoadScene("Credits");
        } else {
            SceneManager.LoadScene("MenuScene");
        }
    }

    void SaveGame() {
        PlayerPrefs.SetInt("level", ScorePreservation.maximumLevel);
        PlayerPrefs.SetInt("luminas", ScorePreservation.totalLuminas);
        int total = PlayerPrefs.GetInt("totalScore");
        PlayerPrefs.SetInt("totalScore", total + ScorePreservation.addScore);

        for (int i = 0; i < ScorePreservation.levelsScore.Length; i++) {
            PlayerPrefs.SetInt("score" + i, ScorePreservation.levelsScore[i]);
            PlayerPrefs.SetInt("progress" + i, ScorePreservation.levelsProgress[i]);
            PlayerPrefs.SetFloat("difficulty" + i, ScorePreservation.levelsDifficulty[i]);
        }
    }
}
